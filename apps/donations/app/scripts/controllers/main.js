'use strict';

/**
 * @ngdoc function
 * @name donateAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the donateAppApp
 */
angular.module('donateAppApp')
  .controller('MainCtrl', function ($scope,$firebase,$http,$location) {

    var ref        = new Firebase('https://donationapp.firebaseio.com/users');
    var sync       = $firebase(ref);
    var syncObject = sync.$asArray();

    $scope.users   = sync.$asArray();

    // Default state to CA
    $scope.state   = 'CA';

    $scope.submitDonation = function(){

      var data ={
        firstName: $scope.firstName,
        middleName: $scope.middleName,
        lastName: $scope.lastName,
        mobile: $scope.mobile,
        email: $scope.email,
        street: $scope.street,
        city: $scope.city,
        state: $scope.state,
        zip: $scope.zip,
        occupation: $scope.occupation,
        employer: $scope.employer
      };

      $scope.users.$add( data );

      // Redirect to choose vendors
      window.location = 'http://do.nr/q00R';
    };
  });
