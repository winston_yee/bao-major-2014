$(document).ready(function(){


	// RIGHT ARROW BOUNCE 1
	var $square = $("#bounce1");
	bounce();
	function bounce() {
	    $square.animate({
	        left: "+=10"
	    }, 800, function() {
	        $square.animate({
	            left: "-=10"
	        }, 800, function() {
	            bounce();
	        })
	    });
	}
	// END RIGHT ARROW BOUNCE 1

	// RIGHT ARROW BOUNCE 2
	var $square2 = $("#bounce2");
	bounce2();
	function bounce2() {
	    $square2.animate({
	        left: "+=10"
	    }, 800, function() {
	        $square2.animate({
	            left: "-=10"
	        }, 800, function() {
	            bounce2();
	        })
	    });
	}
	// END RIGHT ARROW BOUNCE 2

	// RIGHT ARROW BOUNCE 3
	var $square3 = $("#bounce3");
	bounce3();
	function bounce3() {
	    $square3.animate({
	        left: "+=10"
	    }, 800, function() {
	        $square3.animate({
	            left: "-=10"
	        }, 800, function() {
	            bounce3();
	        })
	    });
	}
	// END RIGHT ARROW BOUNCE 3

	// RIGHT ARROW BOUNCE 4
	var $square4 = $("#bounce4");
	bounce4();
	function bounce4() {
	    $square4.animate({
	        left: "+=10"
	    }, 800, function() {
	        $square4.animate({
	            left: "-=10"
	        }, 800, function() {
	            bounce4();
	        })
	    });
	}
	// END RIGHT ARROW BOUNCE 4

	$('#fullpage').fullpage({
		anchors: ['firstPage', 'secondPage', '3rdPage'],
		slidesColor: ['#f3f3f3', '#1BBC9B', '#7E8F7C'],
		css3: true,
		fixedElements: false			
	});




	$("#demosMenu").change(function(){
	  var id = $(this).find("option:selected").attr("id");

	  switch (id){
		case 'background':
		  window.location.href = 'backgrounds.html';
		  break;
		  
		 case 'looping':
		  window.location.href = 'looping.html';
		  break;
		  
		case 'noAnchor':
		  window.location.href = 'noAnchor.html';
		  break;

		case 'scrollingSpeed':
		  window.location.href = 'scrollingSpeed.html';
		  break;

		case 'easing':
		  window.location.href = 'easing.html';
		  break;	

		case 'callbacks':
		  window.location.href = 'callback.html';
		  break;

	    case 'css3':
	      window.location.href = 'css3.html';
	      break;

	    case 'continuous':
	      window.location.href = 'continuous.html';
	      break;

		case 'backgroundVideo':
	      window.location.href = 'videoBackground.html';
	      break;	 

		case 'normalScroll':
	      window.location.href = 'normalScroll.html';
	      break;	 	   

	    case 'scrolling':
	      window.location.href = 'scrolling.html';
	      break;	

	    case 'navigationV':
	      window.location.href = 'navigationV.html';
	      break;		      

	    case 'navigationH':
	      window.location.href = 'navigationH.html';
	      break;	

	    case 'fixedHeaders':
	    	window.location.href = 'fixedHeaders.html';
	    	break;

	    case 'gradientBackgrounds':
	    	window.location.href = 'gradientBackgrounds.html';
	    	break;	    	

	    case 'apple':
	    	window.location.href = 'apple.html';
	    	break;
	  }
	});

});